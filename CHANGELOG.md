# Revision history for basalt

## Unreleased
  - Use static store paths for external executables.
  - Added a `--target` option for specifying `nixos` or
    `home-manager`. When none is specified, Basalt will look for
    either `configuration.nix` or `home.nix` to determine NixOS or
    Home Manager respectively.
  - Finished the `switch` command for switching configurations
  - Added a `build --test` command and option for testing
    configurations. This uses worktree contents, instead of committed
    contents.
  - Switched to obelisk for nixpkgs pinning
  - Show git status in -V
  - When using Home Manager, avoid loading configuration from
    $HOME/.config/nixpkgs or $HOME/.nixpkgs.
  - Added an `update` command for automatically bumping and commiting thunks
  * Add qemu test script in `/test/auto` that tests git-based workflow
  - Add `--root` option for specifying where the root directory is

## 0.1.0.0 -- 2020-02-10

First version in Haskell.  No functionality, simply the cabal-install skeleton.
