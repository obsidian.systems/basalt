{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
-- | Template Haskell that splices in the version info gotten from the nix-provided 'basalt-version' script
module Basalt.Version.TH where

import Control.Exception
import Language.Haskell.TH
import System.Exit
import System.Process
import System.Which

-- | Path of the script we run at compile time to get the version info
versionScript :: FilePath
versionScript = $(staticWhich "basalt-version")

-- | If we couldn't get the version for whatever reason from the script,
-- we will use this version string.
fallbackVersion :: String
fallbackVersion = "UNKNOWN @ UNKNOWN"

-- | Splice of the string representing the basalt version
versionSplice :: ExpQ
versionSplice = stringE =<< do
  result <- runIO $ fmap Right (readProcessWithExitCode versionScript [] "")
    `catch` (\(e :: SomeException) -> pure $ Left (displayException e))
  case result of
    Left err -> reportWarning err >> pure fallbackVersion
    Right (code, out, _err) -> do
      case code of
        ExitSuccess -> pure ()
        _ -> reportWarning $ unwords [ "Script", versionScript, "returned a non-zero exit code:", show code ]
      pure $ takeWhile (/= '\n') out
