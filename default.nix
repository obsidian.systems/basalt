let
  nix-thunk = import ./dep/nix-thunk {};
  sources = nix-thunk.mapSubdirectories nix-thunk.thunkSource ./dep;
  hackGet = nix-thunk.thunkSource;

  obelisk = import sources.obelisk {};

  gitignore = obelisk.nixpkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore";
    rev = "637db329424fd7e46cf4185293b9cc8c88c95394";
    sha256 = "sha256-HG2cCnktfHsKV0s4XW83gU3F57gaTljL9KNSuG6bnQs=";
  };
  gitignoreSource = (import gitignore {}).gitignoreSource;

  basalt-version = let
    refs = obelisk.nixpkgs.lib.sourceByRegex ./.git ["refs.*"];
    parseVersion = obelisk.nixpkgs.writeShellScriptBin "basalt-version" ''
      set -euo pipefail
      HEAD=($(cat "${./.git/HEAD}"))
      if [ "''${HEAD[0]}" = "ref:" ]; then
        REF="''${HEAD[1]##*/}"
        HASH="$(cd "${refs}" && cat "''${HEAD[1]}")"
      else
        REF="HEAD"
        HASH="''${HEAD[0]}"
      fi
      RESULT="$REF @ $HASH"
      echo "$RESULT"
    '';
  in parseVersion;

  # Define GHC compiler override
  pkgs = obelisk.obelisk.override {
    overrides = self: super: with obelisk.nixpkgs; {
      inherit (import (sources.obelisk + "/haskell-overlays/misc-deps.nix") { inherit hackGet; } self super)
        cli-extras
        nix-thunk;

      basalt = haskell.lib.overrideCabal (pkgs.callCabal2nix "basalt" (gitignoreSource ./basalt) {}) (o: {
        buildTools = (o.buildTools or []) ++ [ basalt-version ];
        executableToolDepends = (o.executableToolDepends or []) ++ [ git nix nix-prefetch-git ];
      });
    };
  };
in {
  inherit basalt-version;
  inherit pkgs;
  inherit (pkgs) basalt;
}
