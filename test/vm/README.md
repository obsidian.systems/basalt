# Enter the VM

To run the VM and have access to a console:

```
> ./run.sh
```

By default, the user "test" with password "test" is present.

The VM can be exited with `Ctrl-A x`.

## SSH

You can also SSH into the VM while it's running. By default:

```
ssh -p 2222 test@localhost
```

## Reusing images

The script will use the default qcow2 image name unless otherwise specified. A specific qcow2 can be passed through as well:

```
> ./run.sh basalt-test-copy.qcow2
```

## Git mount

The basalt working directory is mounted in the VM in the test user's home directory.
