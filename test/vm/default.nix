{ vmSshPort ? 2222 }:
let
  nixpkgs = import ../nixpkgs {};

  user = "test";

  basaltMountTarget = "/home/${user}/basalt";
  basaltMountTag = "basalt_working_dir";

  nixosVm = import (nixpkgs.path + /nixos) {
    system = "x86_64-linux";
    configuration = { modulesPath, pkgs, config, ...}: {
      imports = [
        ./configuration.nix
      ];

      # You should be able to bootstrap a system
      # with just git, but the first step should
      # be to install basalt. :)
      environment.systemPackages = [
        pkgs.git
      ];

      services.openssh.enable = true;
      users.users.${user} = {
        isNormalUser = true;
        description = "testing account";
        extraGroups = [ "wheel" ];
        password = "${user}";
        uid = 1000;
      };

      # Mount the working directory in the VM
      # NOTE: The script that starts the VM needs to pass the source as well
      # 1mebibyte for msize should be ok-ish?
      boot.initrd.postMountCommands = ''
        mkdir -p "$targetRoot/${basaltMountTarget}"
        mount -t 9p "${basaltMountTag}" "$targetRoot/${basaltMountTarget}" -o trans=virtio,version=9p2000.L,cache=loose,msize=104857600
      '';
    };
  };

  vmImage = nixosVm.config.system.build.vm;
  systemName = nixosVm.config.system.name;

  enterVmScript = nixpkgs.writeScript "run-vm" ''
    #! /usr/bin/env bash
    set -euxo pipefail

    # An image can be passed explicitly to the script as argument 2, too
    export NIX_DISK_IMAGE="''${2:-}"

    # allows Ctrl+C to pass through the terminal, and Ctrl+A x to terminate QEMU
    # also mounts the basalt working directory in the test user's home
    # This must be matched with boot.initrd.postMountCommands!
    #
    # If not passed, will mount a read-only copy of the source from the host nix store
    export QEMU_OPTS="-serial mon:stdio -virtfs local,path=''${1:-${builtins.toString ../.}},security_model=none,mount_tag=${basaltMountTag}"

    # Expose ssh via host forwarding
    export QEMU_NET_OPTS="hostfwd=tcp::${builtins.toString vmSshPort}-:22"

    # run the VM :)
    "${vmImage}/bin/run-${systemName}-vm"

    # QEMU will garble your terminal settings
    tput smam
  '';

in {
  vm = vmImage;
  script = enterVmScript;
}
