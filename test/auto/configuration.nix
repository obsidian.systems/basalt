{ pkgs, lib, modulesPath, ... }:
{
  # Scaffolding necessary for proper operation of the VM
  imports = [
    (modulesPath + "/virtualisation/qemu-vm.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    (modulesPath + "/testing/test-instrumentation.nix")
  ];

  # Sensible defaults
  virtualisation = {
    memorySize = 8192;
    diskSize = 8192;
    cores = 2;
    useBootLoader = true;
    graphics = false;
  };

  # We don't want the backdoor service to die in the middle of a test
  # just because of an incidental change that would normally trigger
  # its reset.
  systemd.services = {
    backdoor = {
      reloadIfChanged = false;
      restartIfChanged = false;
      stopIfChanged = false;
    };
  };

  # Everything below this line is free to change by basalt

  time.timeZone = "America/New_York";

  environment.systemPackages = with pkgs; [
    wget
    # hello
    git
    gnutar
    gnused
    bash
    unixtools.ping
  ];

  # Add reflex-frp cache
  nix.binaryCaches = [ "https://cache.nixos.org/" "https://nixcache.reflex-frp.org" ];
  nix.binaryCachePublicKeys = [ "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];

  # TODO: This will be necessary for sandboxing
  # nix.binaryCaches = lib.mkForce [];

}
