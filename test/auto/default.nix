let obelisk = import ../../dep/obelisk {};
    rp = obelisk.reflex-platform;
    nixpkgsSrc = rp.hackGet ../nixpkgs;
    nixpkgs = import nixpkgsSrc {};
    basaltSrc = ../..;
    basaltExe = nixpkgs.haskell.lib.justStaticExecutables (import ../..).basalt;
    ob = obelisk.command;
in import (nixpkgsSrc + "/nixos/tests/make-test-python.nix") ({ pkgs, ... } :
{
  skipLint = true;
  machine = import ./configuration.nix;
  testScript =
  let
    log = label: str: "machine.execute('logger -t ${label} ${"\"" + str + "\""}')";
    basaltTarball = pkgs.runCommand "basalt-tar" {} ''
      ${pkgs.gnutar}/bin/tar cvf $out ${basaltSrc}
    '';
    nixpkgsTarball = pkgs.runCommand "nixpkgs-tar" {} ''
      ${pkgs.gnutar}/bin/tar cvf $out ${nixpkgsSrc}
    '';
    configurationNix = ./configuration.nix;
  in ''
    config = "/root/configuration"
    countNixGenerations = "ls -ld /nix/var/nix/profiles/* | wc -l"
    basaltBuild = "cd " + config + " && /root/basalt build --test"

    # Note: the output of basalt commands is not reproducible due to the spinners:
    #       a longer run can have more line churn caused by control characters
    basaltBuildOutputPath = basaltBuild + " | grep -oP '.*\K/nix/store/.*'"

    addHelloPackage = "sed -i 's~# hello~hello~g' " + config + "/configuration.nix"
    removeHelloPackage = "sed -i 's~hello~# hello~g' " + config + "/configuration.nix"
    bareRepo = "/etc/nixos/configuration.git"
    pushConfig = "git -C " + config + " push"
    pullConfig = "git -C " + config + " pull"
    emptyCommit = lambda repo: "git -C " + repo + " commit -m 'empty commit' --allow-empty"
    revParse = lambda repo: "git -C " + repo + " rev-parse HEAD"

    start_all()
    machine.wait_for_unit("default.target")

    ${log "SETUP" "Remounting /boot so that it is writable"}
    machine.succeed("umount /boot")
    machine.succeed("mount /dev/vdb2 /boot")
    machine.succeed("mount | grep /boot | grep rw")

    ${log "SETUP" "Check that nixos binary cache is accessible"}
    machine.succeed("ping -c 1 cache.nixos.org")

    ${log "SETUP" "Configuring git"}
    machine.succeed('git config --global user.email "you@example.com"')
    machine.succeed('git config --global user.name "Your Name"')

    # This seems to make 'import <nixpkgs> {}' work - removing this line makes 'git push' fail with
    ## error: file 'nixpkgs' was not found in the Nix search path (add it using $NIX_PATH or -I), at /tmp/tmp.DQ6iBHnLvt/src/basalt/thunk.nix:10:18
    # Unclear why <nixpkgs> is required here, why it's failing, and why this seemingly fixes it
    machine.succeed("nix-channel --update")

    ${log "SETUP" "Getting basalt and nixpkgs"}
    machine.execute("mkdir -p /tmp/tmp")
    machine.copy_from_host("${basaltTarball}", "/tmp/basalt.tar")
    machine.succeed("tar xvf /tmp/basalt.tar -C /tmp/tmp")
    machine.succeed("mv /tmp/tmp/nix/store/* /tmp/basalt")
    machine.copy_from_host("${nixpkgsTarball}", "/tmp/nixpkgs.tar")
    machine.succeed("tar xvf /tmp/nixpkgs.tar -C /tmp/tmp")
    machine.succeed("mv /tmp/tmp/nix/store/* /tmp/nixpkgs")

    ${log "SETUP" "Setting up basaltified config git repo in /etc/nixos"}
    machine.succeed("mkdir -p /etc/nixos/configuration.git")
    machine.succeed("cp -r /tmp/basalt /etc/nixos/")
    machine.succeed("git -C /etc/nixos/configuration.git init --bare")
    machine.succeed("cd /etc/nixos/configuration.git && rm -r hooks && ln -s ../basalt/targets/nixos/git-hooks hooks")

    ${log "SETUP" "Cloning basaltified config repo"}
    machine.succeed("git clone /etc/nixos/configuration.git /root/configuration")

    ${log "SETUP" "Copying base configuration.nix"}
    machine.copy_from_host("${configurationNix}", "/root/configuration/configuration.nix")

    ${log "SETUP" "Adding nixpkgs dep to configuration repo"}
    machine.succeed("mv /tmp/nixpkgs /root/configuration/")

    ${log "SETUP" "Adding basalt thunk to configuration repo"}
    machine.succeed("git -C /tmp/basalt add .")
    machine.execute("git -C /tmp/basalt commit -m commit-everything")
    machine.succeed("git clone /tmp/basalt /root/configuration/basalt")
    machine.succeed("${ob}/bin/ob thunk pack /root/configuration/basalt")

    ${log "SETUP" "Commit complete config"}
    machine.succeed("git -C /root/configuration add configuration.nix nixpkgs basalt")
    machine.succeed("git -C /root/configuration commit -m commit1")

    ${log "PUSH_TEST" "Testing whether pushing a new config results in new profiles..."}
    ${log "PUSH_TEST" "Retrieve number of existing nix generations"}
    profiles0 = machine.succeed("ls -ld /nix/var/nix/profiles/* | wc -l")

    ${log "PUSH_TEST" "Push configuration to trigger build and activation"}
    machine.succeed(pushConfig)

    ${log "PUSH_TEST" "Retrieve number of nix generations after push"}
    profiles1 = machine.succeed(countNixGenerations)

    assert (int(profiles1) > int(profiles0))
    ${log "PUSH_TEST" "Found new profile! Push test succeeded!"}

    ${log "ACTIVATION_TEST" "Testing whether a pushed config is activated"}

    ${log "ACTIVATION_TEST" "Verifying that the hello command does not exist"}
    hello0 = machine.execute("hello")
    assert (hello0[0] != 0)

    ${log "ACTIVATION_TEST" "Adding pkgs.hello to system configuration"}
    machine.succeed(addHelloPackage)
    machine.succeed("git -C /root/configuration add configuration.nix")
    machine.succeed("git -C /root/configuration commit -m commit2")
    machine.succeed(pushConfig)

    ${log "ACTIVATION_TEST" "Verifying that the hello command is now available"}
    machine.succeed("hello")

    ${log "ACTIVATION_TEST" "Activation test succeeded!"}

    ${log "BASALT_COMMAND" "Installing basalt CLI"}
    ## Installing from fresh state Can take over 10 minutes of downloading and unpacking and
    ## also requires a couple tens of gigabytes of storage so we copy basalt from outside
    # machine.succeed("nix-env -i basalt -f " + config + "/basalt")
    machine.copy_from_host("${basaltExe}/bin/basalt", "/root")
    machine.succeed("/root/basalt --help | grep 'basalt command line application'")

    ${log "BASALT_COMMAND" "Testing \"basalt build\" works"}
    machine.succeed(basaltBuild)

    ${log "BASALT_COMMAND" "Testing \"basalt build\" does not produce new store paths when ran repeatedly"}
    path1 = machine.succeed(basaltBuildOutputPath)
    path2 = machine.succeed(basaltBuildOutputPath)
    assert(path1 == path2)

    ${log "BASALT_COMMAND" "Removing pkgs.hello from system configuration"}
    machine.succeed(removeHelloPackage)

    ${log "BASALT_COMMAND" "Testing \"basalt build\" uses worktree contents instead of committed contents"}
    pathDirty = machine.succeed(basaltBuildOutputPath)
    assert(path2 != pathDirty)

    machine.succeed("git -C " + config + " add " + config + "/configuration.nix")
    machine.succeed("git -C " + config + " commit -m commit3")

    ${log "BASALT_COMMAND" "Testing \"basalt switch\""}
    machine.succeed("cd " + config + " && /root/basalt switch")

    ${log "BASALT_COMMAND" "Retrieve number of existing nix generations"}
    profiles2 = machine.succeed(countNixGenerations)

    ${log "BASALT_COMMAND" "Testing whether \"basalt switch\" resulted in new profiles..."}
    assert (int(profiles2) > int(profiles1))

    ${log "BASALT_COMMAND" "Verifying that the hello command does not exist"}
    assert(0 != machine.execute("hello")[0])

    ${log "BASALT_COMMAND" "Test \"basalt update\" on non-bare repo"}
    machine.succeed(emptyCommit("/tmp/basalt"))
    emptyCommit0 = machine.succeed(revParse("/tmp/basalt"))
    machine.succeed("cd " + config + " && /root/basalt update")
    machine.succeed(pushConfig)
    machine.succeed("cat " + config + "/basalt/git.json | grep " + emptyCommit0)

    ${log "BASALT_COMMAND" "Test \"basalt update\" on bare repo"}
    machine.succeed(emptyCommit("/tmp/basalt"))
    emptyCommit1 = machine.succeed(revParse("/tmp/basalt"))
    machine.succeed("cd " + bareRepo + " && /root/basalt update --target nixos")
    machine.succeed(pullConfig)
    machine.succeed("cat " + config + "/basalt/git.json | grep " + emptyCommit1)

    ${log "BASALT_COMMAND" "Check symlinks loops do not break \"basalt update\""}
    machine.succeed("cd " + config + " && ln -s garbage-symlink garbage-symlink")
    machine.succeed("cd " + config + " && /root/basalt update")
    machine.succeed("cd " + config + " && rm garbage-symlink")

    ${log "BASALT_COMMAND" "Check broken symlinks do not break \"basalt update\""}
    machine.succeed("cd " + config + " && ln -s garbage-symlink-fake-target garbage-symlink")
    machine.succeed("cd " + config + " && /root/basalt update")
    machine.succeed("cd " + config + " && rm garbage-symlink")

    ${log "BASALT_COMMAND" "Test backwards compatibility with basalt as unpacked thunk"}
    machine.succeed("${ob}/bin/ob thunk unpack /root/configuration/basalt")
    machine.succeed("git -C " + config + " add basalt")
    machine.succeed(addHelloPackage)
    machine.succeed("git -C " + config + " add " + config + "/configuration.nix")
    machine.succeed("git -C " + config + " commit -m commit4")
    machine.succeed("cd " + config + " && /root/basalt switch")
    assert(0 == machine.execute("hello")[0])
  '';
})
